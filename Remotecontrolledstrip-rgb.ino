
#include <IRremote.h>

#include <IRremoteInt.h>

int before;
int i;
int out=9; //connect your LED to pin 9 
int steps[]={10,10,10}; //dimmer steps, vary those to increase/decrease the steps between full brightness and turned off
int RECV_PIN = 11; //data out of IR receiver connects to pin 11
int pins[] = {10,9,6};
int bright[] = {255,255,255};
int estado[] = {1,0,0};

unsigned long lastCommand;
unsigned long currentCommand;
unsigned long repeatCommand = 0xFFFFFFFF;

unsigned long NUM_0 = 0x20DF08F7;
unsigned long NUM_1 = 0x20DF8877;
unsigned long NUM_2 = 0x20DF48B7;
unsigned long NUM_3 = 0x20DFC837;
unsigned long NUM_4 = 0x20DF28D7;
unsigned long NUM_5 = 0x20DFA857;
unsigned long NUM_6 = 0x20DF6897;
unsigned long NUM_7 = 0x20DFE817;
unsigned long NUM_8 = 0x20DF18E7;
unsigned long NUM_9 = 0x20DF9867;

unsigned long DIR_UP = 0x20DF02FD;
unsigned long DIR_DOWN = 0x20DF827D;
unsigned long DIR_LEFT = 0x20DFE01F;
unsigned long DIR_RIGHT = 0x20DF609F;

unsigned long PLAY = 0x20DF0DF2;
unsigned long REWIND = 0x20DFF10E;
unsigned long FORWARD = 0x20DF718E;
unsigned long ECO = 0x20DFA956; // Pulsar
unsigned long PAUSE = 0x20DF5DA2;

unsigned long VOL_UP = 0x20DF40BF;
unsigned long VOL_DOWN = 0x20DFC03F;
unsigned long CH_UP = 0x20DF00FF;
unsigned long CH_DOWN = 0x20DF807F;

int PULSAR_DELAY[] = {100, 100, 100};
int PULSAR_SENTIDO[] = {0,0,0};
int pinoAtual = 0;
boolean PULSAR[] = {false, false, false};
boolean FLASH = false;

IRrecv irrecv(RECV_PIN);

decode_results results;



void setup(){
  Serial.begin(9600);
  irrecv.enableIRIn(); // start the receiver
  before=0; //LED is turned off      
    pinMode(pins[0], OUTPUT);
    pinMode(pins[1], OUTPUT);
    pinMode(pins[2], OUTPUT);
  
  digitalWrite(pins[pinoAtual], HIGH);
}


void loop() {

  
  if(PULSAR){
    pulsarFunction();
  } 
  if(FLASH){
    flashFunction();
  }
  
  if (irrecv.decode(&results)) {

    currentCommand = results.value;    
    
    if (currentCommand==PLAY){
      ligarDesligar();
    }

    if(currentCommand == DIR_RIGHT || (currentCommand==repeatCommand && lastCommand==DIR_RIGHT)){
      alterarPino();
      analogWrite(pins[pinoAtual], bright[pinoAtual]);
    }
  
    if(currentCommand == VOL_UP|| (currentCommand==repeatCommand && lastCommand==VOL_UP)){
      aumentarDelay();
    }
    
    if(currentCommand == VOL_DOWN|| (currentCommand==repeatCommand && lastCommand==VOL_DOWN)){
      diminuirDelay();
    }
  
    if(currentCommand == CH_UP|| (currentCommand==repeatCommand && lastCommand==CH_UP)){
      aumentarSteps();
    }
    
    if(currentCommand == CH_DOWN|| (currentCommand==repeatCommand && lastCommand==CH_DOWN)){
      diminuirSteps();
    }
    
  	if(currentCommand==FORWARD || (currentCommand==repeatCommand && lastCommand==FORWARD)){
   		aumentar(pinoAtual);
  	}
    if(currentCommand==REWIND || (currentCommand==repeatCommand && lastCommand==REWIND)){
      diminuir(pinoAtual);
    }
    if(currentCommand==ECO){      
      FLASH=false;
      ligarDesligarPulsar(pinoAtual);
    }
    if (currentCommand==PAUSE){
      PULSAR[pinoAtual]=false;
      ligarDesligarFlash();
    }
    
    if (results.decode_type == NEC) {
      Serial.print("NEC: ");
    } else if (results.decode_type == SONY) {
      Serial.print("SONY: ");
    } else if (results.decode_type == RC5) {
      Serial.print("RC5: ");
    } else if (results.decode_type == RC6) {
      Serial.print("RC6: ");
    } else if (results.decode_type == UNKNOWN) {
      Serial.print("UNKNOWN: ");
    } 

    if(currentCommand!=repeatCommand){
      lastCommand = results.value;
    }

    Serial.println("-------------------");
    Serial.println(results.value, HEX);
    Serial.print("Comando corrente: ");
    Serial.println(currentCommand, HEX);
    Serial.print("Pino Atual: ");
    Serial.println(pinoAtual);
    Serial.print("Quantidade de pinos: ");
    Serial.println(sizeof(pins));
    Serial.print("Ultimo commando: ");
    Serial.println(lastCommand, HEX);
    Serial.println("-------------------");
    irrecv.resume(); // Receive the next value
  }
}

void ligarDesligar(){
  resetFunction();
  Serial.println(digitalRead(out));
  if(digitalRead(pins[pinoAtual])>0){
    digitalWrite(pins[pinoAtual], LOW);
    bright[pinoAtual]=0;
  } else {
    digitalWrite(pins[pinoAtual], HIGH);
    bright[pinoAtual]=255;
  } 
}

void alterarPino(){
  nextPin();
  flashFunction();
}

void nextPin(){
  if((pinoAtual+1)<(sizeof(pins)/2)){
    pinoAtual++;
  } else {
    pinoAtual=0;
  }
}

void aumentar(int pino){
  if(bright[pino]+255/steps[pino]>255){
    analogWrite(pins[pino],bright[pino]);
    PULSAR_SENTIDO[pino]=0;
  }
  else{    
    bright[pino]=bright[pino]+255/steps[pino];
    analogWrite(pins[pino],bright[pino]);
  }
}

void diminuir(int pino){
  if(bright[pino]-255/steps[pino]<0){ 
    analogWrite(pins[pino],bright[pino]);
    PULSAR_SENTIDO[pino]=1;
  }
  else{
    bright[pino]=bright[pino]-255/steps[pino];
    analogWrite(pins[pino],bright[pino]);
  }
}

void aumentarSteps(){
  steps[pinoAtual] = steps[pinoAtual]+1;
}

void diminuirSteps(){
  steps[pinoAtual] = steps[pinoAtual]-1;
}

void aumentarDelay(){
  PULSAR_DELAY[pinoAtual] = PULSAR_DELAY[pinoAtual] + 10;
}

void diminuirDelay(){
  PULSAR_DELAY[pinoAtual] = PULSAR_DELAY[pinoAtual] - 10;
}

void ligarDesligarPulsar(int pino){
  if(PULSAR[pino]){
    PULSAR[pino]=false;
  } else {
    PULSAR[pino]=true;
  }
}

void ligarDesligarFlash(){
  if(FLASH){
    FLASH=false;
  } else {
    FLASH=true;
  }
}

void pulsarFunction(){
  for(int i=0; i<sizeof(pins)/2; i++){
    if(PULSAR[i]){
      if(PULSAR_SENTIDO[i]==0){
        diminuir(i);
      } else{
        aumentar(i);
      } 
      delay(PULSAR_DELAY[i]);
    }
  }
}

void flashFunction(){
  digitalWrite(pins[pinoAtual], HIGH);
  delay(50);
  digitalWrite(pins[pinoAtual], LOW);
  delay(200);
  digitalWrite(pins[pinoAtual], HIGH);
  delay(50);
  digitalWrite(pins[pinoAtual], LOW);
  
  delay(1000);
}

void resetFunction(){
  PULSAR_DELAY[pinoAtual] = 100;
  PULSAR[pinoAtual] = false;
  steps[pinoAtual] = 10;
  FLASH = false;
}

